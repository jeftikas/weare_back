<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidos".
 *
 * @property int $id
 * @property string $credito
 * @property string $tipo
 * @property string $modalidad
 * @property string $nombre
 * @property string $proposito
 * @property string $extension
 * @property string $paises
 * @property string $idioma
 * @property string $optimizacion
 * @property string $palclave
 * @property string $palsecund
 * @property string $descripcion
 * @property string $publico
 * @property string $perspectiva
 * @property string $categoria
 * @property string $fecha
 */
class Pedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'credito', 'tipo', 'modalidad', 'nombre', 'proposito', 'extension', 'paises', 'idioma', 'optimizacion', 'palclave', 'palsecund', 'descripcion', 'publico', 'perspectiva', 'categoria', 'fecha'], 'required'],
            [['id'], 'integer'],
            [['fecha'], 'safe'],
            [['credito', 'tipo', 'modalidad', 'nombre', 'extension', 'idioma', 'palclave', 'palsecund', 'publico', 'perspectiva'], 'string', 'max' => 30],
            [['proposito', 'paises', 'categoria'], 'string', 'max' => 50],
            [['optimizacion'], 'string', 'max' => 10],
            [['descripcion'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'credito' => 'Credito',
            'tipo' => 'Tipo',
            'modalidad' => 'Modalidad',
            'nombre' => 'Nombre',
            'proposito' => 'Proposito',
            'extension' => 'Extension',
            'paises' => 'Paises',
            'idioma' => 'Idioma',
            'optimizacion' => 'Optimizacion',
            'palclave' => 'Palclave',
            'palsecund' => 'Palsecund',
            'descripcion' => 'Descripcion',
            'publico' => 'Publico',
            'perspectiva' => 'Perspectiva',
            'categoria' => 'Categoria',
            'fecha' => 'Fecha',
        ];
    }
}
