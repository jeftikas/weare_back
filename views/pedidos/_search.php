<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\PedidosSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedidos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'credito') ?>

    <?= $form->field($model, 'tipo') ?>

    <?= $form->field($model, 'modalidad') ?>

    <?= $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'proposito') ?>

    <?php // echo $form->field($model, 'extension') ?>

    <?php // echo $form->field($model, 'paises') ?>

    <?php // echo $form->field($model, 'idioma') ?>

    <?php // echo $form->field($model, 'optimizacion') ?>

    <?php // echo $form->field($model, 'palclave') ?>

    <?php // echo $form->field($model, 'palsecund') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'publico') ?>

    <?php // echo $form->field($model, 'perspectiva') ?>

    <?php // echo $form->field($model, 'categoria') ?>

    <?php // echo $form->field($model, 'fecha') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
